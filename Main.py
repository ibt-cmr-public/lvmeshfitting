'''
This is intended to be an easily usable script for segmentation and mesh fitting directly from dicom files
'''

import sys
import os
from utilsForDicom import DicomExam, loadDicomExam
import _pickle as pickle


data_dir = '/Users/buoso/Documents/Data/SNFPigs/' 
datasets = ['P16_T00_con']
stage = 2 # <-- stage 1 = segment, stage 2 = fit mesh (stage 1 must be run before stage 2 can be)
tta = False

if len(sys.argv) > 1:
	stage = int(sys.argv[1])
if stage==1 and len(sys.argv) > 2:
	tta = int(sys.argv[2])

print('running stage %d' % (stage,))

if tta:
	print('using TTA')

if stage == 1:

	for dataset_to_use in datasets:

		input_path = os.path.join(data_dir, dataset_to_use)

		output_path = os.path.join(data_dir, dataset_to_use+'/Output/')
		if not os.path.exists(output_path):
			os.makedirs(output_path)

		de = DicomExam(input_path) #create a dicom exam object, by passing the dicom folder path
		de.standardiseTimeframes() #re-sample data in the time-dimension so that all series have the same number of time steps
		de.segment(use_tta=tta) #segment all the data (possibly using test-time augmentation)
		de.save()
		de.proxyCardiacIndecies('network') #produces a plot of various cardiac indicies (saved as proxy_strains_from_network_predictions.png in the plots folder)
		de.summary() #prints a summary of the data, including its shape after resampling and cropping for segmentation
		de.calculateSegmentationUncertainty()
		de.estimateValvePlanePosition() #heuristically remove predicted mask on atrial slices (eroneously predicted by the segmentation algorithm)
		de.estimateLandmarks()
		de.saveImages(show_landmarks=True) #save a visualisation of the data (+ segmentations if they exist)
		de.save()

if stage == 2:

	for dataset_to_use in datasets:

		input_path = os.path.join(data_dir, dataset_to_use)

		de = loadDicomExam(input_path)
		# de.fitMesh(save_inital_mesh_orbit=True)
		# sys.exit()
		#de.visualiseSlicesIn3D()
		# sys.exit()
		de.resetMeshFitting()
		de.summary()
		# de.fitMesh(training_steps=200, time_frames_to_fit=[0], add_modes_label=True, save_training_progress_gif=True, burn_in_length=0, steps_between_fig_saves=10) #fits a mesh to every time frame. Check the function definition for a list of its arguments
		de.fitMesh(training_steps=50, time_frames_to_fit='all_loop', add_modes_label=True, save_training_progress_gif=False, burn_in_length=0, train_mode='normal',
		mode_loss_weight = 0.05, #how strongly to penalise large mode values
		global_shift_penalty_weigth = 0.3,
		lr = 0.001) #fits a mesh to every time frame. Check the function definition for a list of its arguments
		de.save()
		de.proxyCardiacIndecies('mesh') #produces a plot of various cardiac indicies calculated using the rendered and sliced mesh
		de.calculateSegmentationUncertainty('mesh')
		de.saveImages(use_mesh_images=True)
